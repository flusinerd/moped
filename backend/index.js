const express = require("express");
const app = express();
const Sequelize = require('sequelize');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');
const bodyParser = require('body-parser');
// const faker = require("faker");
const cors = require('cors')
const SSE = require('express-sse');
const sse = new SSE('connected');

app.use(cors());


const sequelize = new Sequelize('moped', 'root', '', {
    host: 'localhost',
    port: '3306',
    dialect: 'mysql',
    operatorsAliases: false,
    logging: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

});

app.get('/api/stream', sse.init);

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const race = sequelize.define("race", {
    name: Sequelize.STRING,
    time: Sequelize.INTEGER
})

const driver = sequelize.define("driver", {
    name: Sequelize.STRING,
    startnumber: Sequelize.INTEGER,
})

race.hasMany(driver);
driver.belongsTo(race);

sequelize.sync({ force: false })
    .then(() => {
    })


app.post('/api/drivers', (req, res) => {
    sse.send('', 'createdDriver');
    console.log(req.body);
    driver.create({name: req.body.name, startnumber: req.body.startnumber})
    .then(async (createdDriver) => {
        let relatedRace = await race.findByPk(req.body.race);
        createdDriver.race = relatedRace;
        await createdDriver.save();
        res.status(201).json(createdDriver);
    })
    .catch((err) => {
        res.status(500).json({err: err});
    })
})

app.post('/api/races', (req, res) =>{
    race.create(req.body)
    .then((createdRace) =>{
        res.status(201).json(createdRace);
        sse.send('', 'createdRace')
    })
    .catch((err) => {
        res.status(500).json({err: err});
    })
})

app.get('/api/races', (req, res) =>{
    race.findAll()
    .then((races) =>{
        res.status(200).json(races);
    })
    .catch((err) =>{
        res.status(500).json({err: err});
    })
})

app.listen(3000, function () {
    console.clear();
    sequelize.authenticate()
        .then(() => {
            console.log("DB connected");
        })
        .catch((err) => {
            console.log("Error while connecting to db", err);
        })
})