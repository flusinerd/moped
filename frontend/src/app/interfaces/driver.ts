export interface driver{
  id:Number;
  name:String,
  startnumber:Number,
  createdAt: Date;
  updatedAt: Date;
}