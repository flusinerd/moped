export interface race{
  id:Number;
  name?:String;
  time?:Number;
  createdAt: Date;
  updatedAt: Date;
}