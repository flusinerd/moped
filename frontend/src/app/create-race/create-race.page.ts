import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RaceService } from '../services/race.service';


@Component({
  selector: 'app-create-race',
  templateUrl: './create-race.page.html',
  styleUrls: ['./create-race.page.scss'],
})
export class CreateRacePage implements OnInit {
  raceName:string;
  hours:number;
  minutes:number;
  seconds:number;
  constructor(
    private modalController:ModalController,
    private _raceService:RaceService,
    )
    { }

  ngOnInit() {
  }

  dismiss(){
    this.modalController.dismiss();
  }

  createRace(){
    if (this.hours === null){
      this.hours = 0;
    }
    if (this.minutes === null){
      this.minutes = 0;
    }
    if (this.seconds === null){
      this.seconds = 0;
    }
    this._raceService.createRace(this.hours, this.minutes, this.seconds, this.raceName);
  }

}
