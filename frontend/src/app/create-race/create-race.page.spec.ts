import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRacePage } from './create-race.page';

describe('CreateRacePage', () => {
  let component: CreateRacePage;
  let fixture: ComponentFixture<CreateRacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRacePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
