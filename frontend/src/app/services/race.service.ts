import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { race } from '../interfaces/race'


@Injectable({
  providedIn: 'root'
})
export class RaceService {

  public races = new BehaviorSubject<Array<race>>([]);

  constructor(private http:HttpClient) {
    this.listenToSSE();
  }

  public createRace(hours:number = 0, minutes:number = 0, seconds:number = 0, raceName){
    let body = {
      name: raceName,
      time: hours*60*60+minutes*60+seconds + '',
    };
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }
    let bodyString = new HttpParams({ fromObject: body }).toString();
    this.http.post('http://localhost:3000/api/races', bodyString, options).toPromise()
    .then((response:race) => {
    })
    .catch((err) => {
      throw err;
    })
  }

  private listenToSSE(){
    let source = new EventSource('http://localhost:3000/api/stream');
    source.addEventListener('createdRace', () =>{
      this.getAllRaces();
    })
  }

  public getAllRaces() {
    this.http.get('http://localhost:3000/api/races').toPromise()
      .then((races: Array<race>) => {
        this.races.next(races);
      })
  }


}
