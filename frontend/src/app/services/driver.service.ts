import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { driver } from '../interfaces/driver'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class DriverService {

  public drivers = new BehaviorSubject<Array<driver>>([]);
  constructor(private http:HttpClient) { }

  public getAllDrivers(){
    this.http.get('http://localhost:3000/api/drivers').toPromise()
    .then((drivers:Array<driver>) => {
      this.drivers.next(drivers);
    })
    .catch((err) => {
      throw err;
    })
  }

  listenToSSE(){
    let source = new EventSource('http://localhost:3000/api/stream')
    source.addEventListener('createdDriver', () =>{
      this.getAllDrivers();
    })
  }

  public createDriver(driverName:string, startNumber:Number, selectedRace:string){
    let body = {
      name: driverName,
      startnumber: startNumber + '',
      race: selectedRace,
    };
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }
    let bodyString = new HttpParams({ fromObject: body }).toString();
    this.http.post('http://localhost:3000/api/drivers', bodyString, options).toPromise()
      .then((response) => {
        console.log(response);
      })
      .catch((err) => {
        throw err;
      })
  }
}
