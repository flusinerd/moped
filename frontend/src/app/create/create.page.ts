import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CreateRacePage } from '../create-race/create-race.page';
import { race } from '../interfaces/race';
import { RaceService } from '../services/race.service';
import { DriverService } from '../services/driver.service';



@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
  races: Array<race>;
  selectedRace: string = "default";
  driverName: string;
  startNumber: number;
  class: number;
  constructor(
    public modalController: ModalController,
    private _raceService: RaceService,
    private _driverService: DriverService) 
    {}

  ngOnInit() {
    this._raceService.getAllRaces();
    this._raceService.races.subscribe((races) => {
      this.races = races;
    })
    
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: CreateRacePage,
      componentProps: {
        '(created)': "console.log('created')",
      }
    })
    return await modal.present();
  }

  createDriver() {
    if (this.selectedRace !== "default") {
      this._driverService.createDriver(this.driverName, this.startNumber, this.selectedRace);
    } else {
      // Present errors
    }
  }
}
